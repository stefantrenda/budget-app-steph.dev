const budgetFeedback = document.querySelector('.budget-feedback');
const expenseFeedback = document.querySelector('.expense-feedback');
const budgetForm = document.querySelector('#budget-form');
const budgetInput = document.querySelector('#budget-input');
const budgetAmount = document.querySelector('#budget-amount');
const expenseAmount = document.querySelector('#expense-amount');
const table = document.querySelector('table tbody');
const balance = document.querySelector('#balance');
const balanceAmount = document.querySelector('#balance-amount');
const amountInput = document.querySelector('#amount-input');
const expenseForm = document.querySelector('#expense-form');
const expenseInput = document.querySelector('#expense-input');
const expenseList = document.querySelector('#expense-list');
const listItem = [];
let listItemId = 0;
let edit_row;


budgetForm.addEventListener('submit', (e) => {
  e.preventDefault();
  if (budgetInput.value < 0 || budgetInput.value === '') {
    budgetFeedback.innerHTML = `Value Cannot Be Emtpy Or Negative`;
    budgetFeedback.classList.add('showItem');
    budgetInput.value = '';
  } else {
    budgetAmount.textContent = budgetInput.value;
    budgetInput.value = '';
    mainBalance();
    
  }
});

budgetInput.addEventListener('focus', () => {
    budgetFeedback.classList.remove('showItem');
  });

const mainBalance = () => {
  let expense = totalExpense();
  const total = parseInt(budgetAmount.textContent) - expense;
  balanceAmount.textContent = total;

  if (total < 0) {
    balance.classList.remove('showGreen', 'showBlack');
    balance.classList.add('showRed');
} else if (total > 0) {
    balance.classList.remove('showRed', 'showBlack');
    balance.classList.add('showGreen');
} else if (total === 0) {
    balance.classList.remove('showRed', 'showGreen');
    balance.classList.add('showBlack');
}
};
const totalExpense = () => {
  let total = 0;

  if (listItem.length > 0) {
    total = listItem.reduce(function (num, total) {
      num += total.amount;
      return num;
    }, 0);
  }
  expenseAmount.textContent = total;
  return total;
};

expenseForm.addEventListener('submit', (e) => {
  e.preventDefault();
  let expenseVal = expenseInput.value;
  let amountVal = amountInput.value;

  if (!expenseVal || !amountVal || amountVal < 0) {
    expenseFeedback.innerHTML = `values cannot be empty or negative`;
    expenseFeedback.classList.add('showItem');
  } else {
    let amount = parseInt(amountVal);
    expenseInput.value = '';
    amountInput.value = '';

    let expense = {
      title: expenseVal,
      id: listItemId,
      amount: amount,
    };
    listItemId++;
    addExpense(expense);
    listItem.push(expense);

    mainBalance();
  }
});

expenseInput.addEventListener('focus', () => {
    expenseFeedback.classList.remove('showItem');
  });
amountInput.addEventListener('focus', () => {
    expenseFeedback.classList.remove('showItem');
  });

  
const addExpense = (expense) => {
  let secondRow = table.insertRow();
  let expenseTitle = secondRow.insertCell(0);
  let expenceAmount = secondRow.insertCell(1);
  let editIcon = secondRow.insertCell(2);
  let deleteIcon = secondRow.insertCell(3);
  secondRow.setAttribute('id', `${expense.id}`);
  expenseTitle.innerHTML = expense.title;
  expenseTitle.classList.add('expense-title');
  expenceAmount.innerHTML = expense.amount;
  expenceAmount.classList.add('expense-amount');
  editIcon.innerHTML = `<a class="edit-icon" > <i class="fas fa-edit"></i> </a>`;
  editIcon.classList.add('edit-icon');
  editIcon.href = '#';
  deleteIcon.innerHTML = `<a class="delete-icon"> <i class="fas fa-trash"></i> </a>`;
  deleteIcon.classList.add('delete-icon');
  deleteIcon.href = '#';
  table.append(secondRow);
};

document.addEventListener('click', (e) => {
  if (e.target.parentElement.classList.contains('edit-icon')) {
    let tr = e.target.parentElement.parentElement.parentElement;
    let index = tr.rowIndex;
    edit_row = tr.rowIndex;
    expenseInput.value = listItem[edit_row - 1].title;
    amountInput.value = listItem[edit_row - 1].amount;
    tr.remove();
    listItem.splice(index - 1, 1);
    mainBalance();
  }
  if (e.target.parentElement.classList.contains('delete-icon')) {
    let tr = e.target.parentNode.parentNode.parentNode;
    let index = tr.rowIndex;
    listItem.splice(index - 1, 1);
    tr.remove();
    mainBalance();
  }
});